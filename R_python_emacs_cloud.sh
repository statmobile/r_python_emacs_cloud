#/bin/bash

BASH='/bin/bash'
DIR=$HOME

echo 'This script will install the following in: ' $DIR
echo 'R base development environment (r-base-core-dbg)'
echo 'A Python 3.4.2 virtual environment'
echo 'Emacs 24.4 as and IDE for both'
echo 'As well as a nice configuration for tmux'

echo 'This will need root privledges so read through the code'

read -p "Do you want to continue [Type in yes]" answer
if [[ $answer = yes ]] ; then
    # run the command
    echo 'You said yes'
else
    exit
fi


